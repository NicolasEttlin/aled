<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->tinyInteger('status')
              ->unsigned()
              ->default(0)
              ->comment('0 = need help, 2 = in progress, 5 = resolved');
            $table->text('description');
            $table->text('solution')->nullable();
            $table->boolean('urgent')->default(false);
            
            // @TODO : replace with a relation
            $table->string('asker');
            $table->string('savior')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issues');
    }
}
