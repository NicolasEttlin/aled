<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    protected $fillable = ['title', 'asker', 'description'];

    private static $status = [
        0 => "Besoin d'aide",
        1 => "En cours",
        2 => "Résolu",
    ];

    public function getStatus() {
        return self::$status[$this->status];
    }

    public function getContent() {
        $parser = new \Parsedown();
        return $parser->text(htmlspecialchars($this->description));
    }
}
