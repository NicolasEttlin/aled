<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'IssuesController@index');
Route::get('issues/new', 'IssuesController@create');
Route::get('issues/{issue}', 'IssuesController@show');
Route::get('issues/{issue}/edit', 'IssuesController@edit');
Route::patch('issues/{issue}', 'IssuesController@update');
Route::put('issues', 'IssuesController@store');
