<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>ALED</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/themes/prism.min.css" rel="stylesheet">
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <nav role="navigation">
            <div class="container">
                <a href="/">ALED</a>
            </div>
        </nav>

        @yield('content')

        <script src="{{ mix('js/app.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/prism.min.js"></script>
    </body>
</html>
