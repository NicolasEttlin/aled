@extends('layout')

@section('content')
    <div class="container home-page">
        <a class="button" href="/issues/new">Demander de l'aide</a>

        <ul class="issues">
            @foreach ($issues as $issue)
                @if ($loop->first || $issues[$loop->index - 1]->status !== $issue->status)
                    <li class="group-header">{{ $issue->getStatus() }}</li>
                @endif
                <li>
                    <a href="issues/{{ $issue->id }}">
                        <strong>{{ $issue->title }}</strong> <em>(par {{ $issue->asker }})</em>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
@endsection
