@extends('layout')

@section('content')
    <div class="container">
        <h1>Créer</h1>

        @if ($errors->any())
            <div class="alert">
                Oups ! Il faut corriger les problèmes suivants pour continuer :
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/issues" method="post" class="issue-form">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <label>
                Titre
                <input type="text" name="title" maxlength="50" autocomplete="off" value="{{ old('title') }}">
            </label>

            <label>
                Nom du demandeur
                <input type="text" name="asker" value="{{ old('asker') }}">
            </label>

            <label>
                Description
                <textarea name="description">{{ old('description') }}</textarea>
            </label>

            <button type="submit">Créer</button>
        </form>
    </div>
@endsection
