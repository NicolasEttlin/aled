@extends('layout')

@section('content')
    <div class="container">
        <h1>Modifier</h1>

        @if ($errors->any())
            <div class="alert">
                Oups ! Il faut corriger les problèmes suivants pour continuer :
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/issues/{{ $issue->id }}" method="post" class="issue-form">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <label>
                Titre
                <input type="text" name="title" maxlength="50" autocomplete="off" value="{{ old('title') ?? $issue->title }}">
            </label>

            <label>
                Nom du demandeur
                <input type="text" name="asker" value="{{ old('asker') ?? $issue->asker }}">
            </label>

            <label>
                Description
                <textarea name="description">{{ old('description') ?? $issue->description }}</textarea>
            </label>

            <div class="buttons">
                <a class="button button-secondary button-small button-cancel" href="/issues/{{ $issue->id }}">Annuler</a>
                <button type="submit">Modifier</button>
            </div>
        </form>
    </div>
@endsection
