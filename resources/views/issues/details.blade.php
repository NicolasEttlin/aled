@extends('layout')

@section('content')
    <div class="issue">
        <header>
            <div class="container">
                <h1>{{ $issue->title }}</h1>
                <small class="author">
                    <img src="https://api.adorable.io/avatars/285/{{ $issue->asker }}">
                    par {{ $issue->asker }}
                </small>

                Status : <div class="status-badge status-{{ $issue->status }}">
                    {{ $issue->getStatus() }}
                </div>
            </div>
        </header>
        <main>
            <div class="container">
                <div class="fab-buttons">
                    <a href="/issues/{{ $issue->id }}/edit"><i class="fa fa-edit"></i></a>
                </div>

                {!! $issue->getContent() !!}
            </div>
        </main>
    </div>
@endsection
